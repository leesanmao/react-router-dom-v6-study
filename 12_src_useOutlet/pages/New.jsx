import React from "react";
import { useNavigationType } from 'react-router-dom'

export default function New() {
  const a = useNavigationType()
  // POP PUSH REPLACE 其中pop表示在当前页面刷新页面
  console.log("🚀 ~ file: New.jsx ~ line 6 ~ New ~ a", a)
  
  return (
    <ul>
      <li>news001</li>
      <li>news002</li>
      <li>news003</li>
    </ul>
  );
}

import React from 'react'
import {useSearchParams, useLocation } from 'react-router-dom'

export default function Detail() {
    const [search, setSearch] = useSearchParams()
    // location里面有search
    const a = useLocation()
    console.log("🚀 ~ file: Detail.jsx ~ line 7 ~ Detail ~ a", a)
    console.log("🚀 ~ file: Detail.jsx ~ line 6 ~ Detail ~ search", search)
    // 需要get他
    const id = search.get('id')
    const title = search.get('title')
    const content = search.get('content')
  return (
    <ul>
        <li><button onClick={()=>setSearch('id=008&title=哈哈&content=西西')}>点我更新一下收到的search参数</button></li>
        <li>消息的编号：{id}</li>
        <li>消息的标题：{title}</li>
        <li>消息的的内容:{content}</li>
    </ul>
  )
}
